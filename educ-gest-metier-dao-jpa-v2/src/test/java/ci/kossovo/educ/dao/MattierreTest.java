package ci.kossovo.educ.dao;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import ci.kossovo.educ.entity.Matiere;

@RunWith(SpringRunner.class)
@DataJpaTest
public class MattierreTest {
	@Autowired
	TestEntityManager entityManager;
	@Autowired
	MatiereRepository matiereRepository;

	@Test
	public void findAllAndfindOneAndsave() {
		entityManager.persist(new Matiere("Java", "language"));
		entityManager.persist(new Matiere("Ruby", "language"));
		entityManager.persist(new Matiere("Groovy", "language"));
		
		List<Matiere> mat1 = matiereRepository.findAll();
		assertThat(mat1.size()).isEqualTo(3);
		Long id = mat1.get(0).getId();
		Matiere m = matiereRepository.findById(id).get();
		assertNotNull(m);
		
		m.setLibelle("Java 8");
		m = matiereRepository.save(m);
		assertThat(m.getLibelle()).isEqualTo("Java 8");
		matiereRepository.deleteById(id);
		assertThat(matiereRepository.findAll().size()).isEqualTo(2);
		assertFalse(matiereRepository.findById(id).isPresent());
		assertNotNull(matiereRepository.save(new Matiere("Spring", "Java framework")));
		assertThat(matiereRepository.findAll().size()).isEqualTo(3);
	}

}
