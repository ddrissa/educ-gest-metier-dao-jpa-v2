package ci.kossovo.educ.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import ci.kossovo.educ.entity.TypeEvaluation;

public interface TtpeEvaluationRepository extends JpaRepository<TypeEvaluation, Long> {

}
