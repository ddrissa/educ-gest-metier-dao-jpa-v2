package ci.kossovo.educ.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import ci.kossovo.educ.entity.Filiere;

public interface FiliereRepository extends JpaRepository<Filiere, Long> {

}
