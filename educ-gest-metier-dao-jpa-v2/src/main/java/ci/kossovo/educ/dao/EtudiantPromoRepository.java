package ci.kossovo.educ.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import ci.kossovo.educ.entity.EtudiantPomo;
import ci.kossovo.educ.entity.EtudiantPromoID;

public interface EtudiantPromoRepository extends JpaRepository<EtudiantPomo, EtudiantPromoID> {

}
