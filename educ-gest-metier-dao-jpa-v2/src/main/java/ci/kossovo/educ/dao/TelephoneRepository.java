package ci.kossovo.educ.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import ci.kossovo.educ.entity.Telephone;

public interface TelephoneRepository extends JpaRepository<Telephone, Long> {

}
