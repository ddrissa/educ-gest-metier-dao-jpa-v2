package ci.kossovo.educ.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import ci.kossovo.educ.entity.Enseigne;
import ci.kossovo.educ.entity.EnseigneID;

public interface EnseigneRepository extends JpaRepository<Enseigne, EnseigneID> {

}
