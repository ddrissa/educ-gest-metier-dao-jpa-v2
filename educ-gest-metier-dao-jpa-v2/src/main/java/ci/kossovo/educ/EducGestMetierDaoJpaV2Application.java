package ci.kossovo.educ;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EducGestMetierDaoJpaV2Application {

	public static void main(String[] args) {
		SpringApplication.run(EducGestMetierDaoJpaV2Application.class, args);
	}
}
