package ci.kossovo.educ.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "T_TELEPHOME")
// @Data
public class Telephone extends AbstractEntity {
	private static final long serialVersionUID = 1L;

	private String titre;
	private String numero;

	public Telephone() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Telephone(Long id, Long version, String titre, String numero) {
		super(id, version);
		this.titre = titre;
		this.numero = numero;
	}

	@Override
	public String toString() {
		return String.format("Telephone[%s,%s,%s,%s]", id, version, titre, numero);
	}

	public String getTitre() {
		return titre;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

}
