//
// This file was generated by the JPA Modeler
//
package ci.kossovo.educ.entity;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="T_USERS_ROLES")
public class UserRole extends AbstractEntity{
	private static final long serialVersionUID = 1L;

	@ManyToOne
	@JoinColumn(name="ROLES_ID")
	private Role role;

	@ManyToOne
	@JoinColumn(name="PERSONNE_ID")
	private Personne personne;

	public UserRole() {

	}

	public Role getRole() {
		return this.role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Personne getPersonne() {
		return personne;
	}

	public void setPersonne(Personne personne) {
		this.personne = personne;
	}

	

	

}
