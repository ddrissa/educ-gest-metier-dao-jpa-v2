package ci.kossovo.educ.entity;

import java.util.List;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "T_ENSEIGNANT")
@DiscriminatorValue("EN")
public class Enseignant extends Personne {
	private static final long serialVersionUID = 1L;

	private String status;
	private String specialite;
	/*
	 * @ElementCollection
	 * 
	 * @CollectionTable(name="telephone")
	 * 
	 * @MapKeyColumn(name="type")
	 * 
	 * @Column(name="numero") private Map<String, String> telephone = new
	 * HashMap<>();
	 */

	public Enseignant() {

	}

	public Enseignant(Long id, String titre, String nom, String prenom, String numCni, String status,
			String specialite) {
		super(id, titre, nom, prenom, numCni);
		this.status = status;
		this.specialite = specialite;

	}

	public Enseignant(String titre, String nom, String prenom, String status) {
		super(titre, nom, prenom);
		this.status = status;
	}

	public Enseignant(String titre, String nom, String prenom, String numCni, String status) {
		super(titre, nom, prenom, numCni);
		this.status = status;
	}

	@Override
	public String toString() {
		return String.format("Enseignant[%s]", super.toString());
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getSpecialite() {
		return specialite;
	}

	public void setSpecialite(String specialite) {
		this.specialite = specialite;
	}

}
