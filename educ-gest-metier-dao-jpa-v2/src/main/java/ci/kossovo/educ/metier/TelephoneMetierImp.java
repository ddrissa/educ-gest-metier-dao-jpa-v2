package ci.kossovo.educ.metier;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import ci.kossovo.educ.dao.TelephoneRepository;
import ci.kossovo.educ.entity.Telephone;
import ci.kossovo.educ.exceptions.InvalidEducException;

public class TelephoneMetierImp implements ITelephoneMetier {
	@Autowired
	private TelephoneRepository telephoneRepository;

	@Override
	public Telephone creer(Telephone entity) throws InvalidEducException {
		
		return telephoneRepository.save(entity);
	}

	@Override
	public Telephone modifier(Telephone entity) throws InvalidEducException {
		return telephoneRepository.save(entity);
	}

	@Override
	public Telephone find(Long id) {
		return telephoneRepository.findById(id).get();
	}

	@Override
	public List<Telephone> findAll() {
		return telephoneRepository.findAll();
	}

	@Override
	public void spprimer(List<Telephone> entities) {
		telephoneRepository.deleteInBatch(entities);
		
	}

	@Override
	public boolean supprimer(Long id) {
		telephoneRepository.deleteById(id);
		return true;
	}
	

	@Override
	public boolean existe(Long id) {
		return telephoneRepository.existsById(id);
	}

	@Override
	public Long compter() {
		return telephoneRepository.count();
	}

}
