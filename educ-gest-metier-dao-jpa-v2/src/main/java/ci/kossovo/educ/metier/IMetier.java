package ci.kossovo.educ.metier;

import java.util.List;

import ci.kossovo.educ.exceptions.InvalidEducException;

public interface IMetier<T, U> {

	public T creer(T entity) throws InvalidEducException;

	public T modifier(T entity) throws InvalidEducException;

	public T find(U id);

	public List<T> findAll();

	public void spprimer(List<T> entities);

	public boolean supprimer(U id);

	public boolean existe(U id);

	public Long compter();

}
