package ci.kossovo.educ.metier;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ci.kossovo.educ.dao.FiliereRepository;
import ci.kossovo.educ.entity.Filiere;

@Service 
public class FiliereMetierImpl implements IFiliereMetier {

	@Autowired
	private FiliereRepository filiereRepository;
	
	@Override
	public Filiere creer(Filiere entity) {
		return filiereRepository.save(entity);
	}

	@Override
	public Filiere modifier(Filiere entity) {
		return filiereRepository.save(entity);
	}

	@Override
	public Filiere find(Long id) {
		return filiereRepository.findById(id).get();
	}

	@Override
	public List<Filiere> findAll() {
		return filiereRepository.findAll();
	}

	@Override
	public void spprimer(List<Filiere> entities) {
		filiereRepository.deleteAll(entities);

	}

	@Override
	public boolean supprimer(Long id) {
		filiereRepository.deleteById(id);
		return true;
	}

	@Override
	public boolean existe(Long id) {
		return filiereRepository.existsById(id);
	}

	@Override
	public Long compter() {
		return filiereRepository.count();
	}

}
